//
// Created by Andi on 05.02.2018.
//

#ifndef SFML_TEST_CATEGORY_H
#define SFML_TEST_CATEGORY_H

#endif //SFML_TEST_CATEGORY_H


namespace Category {
    enum Type {
        None = 0,
        Scene = 1 << 0,
        PlayerAircraft = 1 << 1,
        AlliedAircraft = 1 << 2,
        EnemyAircraft = 1 << 3,
    };
}