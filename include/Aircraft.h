//
// Created by Andi on 03.02.2018.
//

#ifndef SFML_TEST_AIRCRAFT_H
#define SFML_TEST_AIRCRAFT_H


#include <SFML/Graphics/Sprite.hpp>
#include "Entity.h"
#include "ResourceIdentifiers.h"


class Aircraft : public Entity {
public:
    enum Type {
        Eagle,
        Raptor,
    };


public:
    Aircraft(Type type, const TextureHolder &textures);

    virtual void drawCurrent(sf::RenderTarget &target, sf::RenderStates states) const;

    virtual unsigned int getCategory() const;

private:
    Type mType;
    sf::Sprite mSprite;
};


#endif //SFML_TEST_AIRCRAFT_H
