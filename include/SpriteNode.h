//
// Created by Andi on 03.02.2018.
//

#ifndef SFML_TEST_SPRITENODE_H
#define SFML_TEST_SPRITENODE_H


#include "SceneNode.h"

#include <SFML/Graphics/Sprite.hpp>


class SpriteNode : public SceneNode {
public:
    explicit SpriteNode(const sf::Texture &texture);

    SpriteNode(const sf::Texture &texture, const sf::IntRect &textureRect);


private:
    virtual void drawCurrent(sf::RenderTarget &target, sf::RenderStates states) const;


private:
    sf::Sprite mSprite;
};


#endif //SFML_TEST_SPRITENODE_H
