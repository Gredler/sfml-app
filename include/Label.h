//
// Created by Andi on 12.02.2018.
//

#ifndef SFML_TEST_LABEL_H
#define SFML_TEST_LABEL_H


#include <SFML/Graphics/Text.hpp>
#include <SFML/Window/Event.hpp>
#include "ResourceIdentifiers.h"
#include "Component.h"

#include <SFML/Graphics/Text.hpp>

namespace GUI {

    class Label : public Component {
    public:
        typedef std::shared_ptr<Label> Ptr;


    public:
        Label(const std::string &text, const FontHolder &fonts);

        virtual bool isSelectable() const;

        void setText(const std::string &text);

        virtual void handleEvent(const sf::Event &event);


    private:
        void draw(sf::RenderTarget &target, sf::RenderStates states) const;


    private:
        sf::Text mText;
    };

}


#endif //SFML_TEST_LABEL_H
