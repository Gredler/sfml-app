//
// Created by Andi on 06.02.2018.
//

#ifndef SFML_TEST_STATESTACK_H
#define SFML_TEST_STATESTACK_H


#include "State.h"
#include "StateIdentifiers.h"
#include "ResourceIdentifiers.h"

#include <SFML/System/NonCopyable.hpp>
#include <SFML/System/Time.hpp>

#include <vector>
#include <utility>
#include <functional>
#include <map>


namespace sf {
    class Event;

    class RenderWindow;
}

class StateStack : private sf::NonCopyable {
public:
    enum Action {
        Push,
        Pop,
        Clear,
    };


public:
    explicit StateStack(State::Context context);

    template<typename T>
    void registerState(States::ID stateID);

    void update(sf::Time dt);

    void draw();

    void handleEvent(const sf::Event &event);

    void pushState(States::ID stateID);

    void popState();

    void clearStates();

    bool isEmpty() const;


private:
    State::Ptr createState(States::ID stateID);

    void applyPendingChanges();


private:
    struct PendingChange {
        explicit PendingChange(Action action, States::ID stateID = States::None);

        Action action;
        States::ID stateID;
    };


private:
    std::vector<State::Ptr> mStack;
    std::vector<PendingChange> mPendingList;

    State::Context mContext;
    std::map<States::ID, std::function<State::Ptr()>> mFactories;
};


template<typename T>
void StateStack::registerState(States::ID stateID) {
    // the arrow tells the lambda expression to use that specific type as return type
    // amazing
    mFactories[stateID] = [this]() -> std::unique_ptr<State> {
        return State::Ptr(new T(*this, mContext));
    };
}


#endif //SFML_TEST_STATESTACK_H
