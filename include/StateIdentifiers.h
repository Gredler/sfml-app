//
// Created by Andi on 06.02.2018.
//

#ifndef SFML_TEST_STATEIDENTIFIERS_H
#define SFML_TEST_STATEIDENTIFIERS_H

namespace States {
    enum ID {
        None,
        Title,
        Menu,
        Game,
        Loading,
        Pause,
        Settings
    };
}


#endif //SFML_TEST_STATEIDENTIFIERS_H
