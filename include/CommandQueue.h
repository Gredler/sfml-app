//
// Created by Andi on 05.02.2018.
//

#ifndef SFML_TEST_COMMANDQUEUE_H
#define SFML_TEST_COMMANDQUEUE_H


#include "Command.h"

#include <queue>


class CommandQueue {
public:
    void push(const Command &command);

    Command pop();

    bool isEmpty() const;


private:
    std::queue<Command> mQueue;
};


#endif //SFML_TEST_COMMANDQUEUE_H
