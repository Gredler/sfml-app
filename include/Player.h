//
// Created by Andi on 05.02.2018.
//

#ifndef SFML_TEST_PLAYER_H
#define SFML_TEST_PLAYER_H


#include <SFML/Window/Event.hpp>
#include <map>
#include "CommandQueue.h"

#include <SFML/Window/Event.hpp>

#include <map>


class CommandQueue;

class Player {
public:
    enum Action {
        MoveLeft,
        MoveRight,
        MoveUp,
        MoveDown,
        ActionCount
    };


public:
    Player();

    void handleEvent(const sf::Event &event, CommandQueue &commands);

    void handleRealtimeInput(CommandQueue &commands);

    void assignKey(Action action, sf::Keyboard::Key key);

    sf::Keyboard::Key getAssignedKey(Action action) const;


private:
    void initializeActions();

    static bool isRealtimeAction(Action action);


private:
    std::map<sf::Keyboard::Key, Action> mKeyBinding;
    std::map<Action, Command> mActionBinding;
};


#endif //SFML_TEST_PLAYER_H
