//
// Created by Andi on 12.02.2018.
//

#ifndef SFML_TEST_CONTAINER_H
#define SFML_TEST_CONTAINER_H


#include <memory>
#include <SFML/System/NonCopyable.hpp>
#include <SFML/Graphics/Transformable.hpp>
#include <SFML/Graphics/Drawable.hpp>

#include <vector>
#include <memory>
#include "Component.h"


namespace GUI {

    class Container : public Component {
    public:
        typedef std::shared_ptr<Container> Ptr;


    public:
        Container();

        void pack(Component::Ptr component);

        virtual bool isSelectable() const;

        virtual void handleEvent(const sf::Event &event);


    private:
        virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const;

        bool hasSelection() const;

        void select(std::size_t index);

        void selectNext();

        void selectPrevious();


    private:
        std::vector<Component::Ptr> mChildren;
        int mSelectedChild;
    };

}


#endif //SFML_TEST_CONTAINER_H
