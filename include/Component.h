//
// Created by Andi on 12.02.2018.
//

#ifndef SFML_TEST_COMPONENT_H
#define SFML_TEST_COMPONENT_H


#include <memory>
#include <SFML/System/NonCopyable.hpp>
#include <SFML/Graphics/Transformable.hpp>
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Window/Event.hpp>

namespace GUI {
    class Component : public sf::Drawable, public sf::Transformable, private sf::NonCopyable {
    public:
        typedef std::shared_ptr<Component> Ptr;
    public:
        Component();

        virtual ~Component();

        virtual bool isSelectable() const = 0;

        bool isSelected() const;

        virtual void select();

        virtual void deselect();

        virtual bool isActive() const;

        virtual void activate();

        virtual void deactivate();

        virtual void handleEvent(const sf::Event &event) = 0;

    private:
        bool mIsSelected;
        bool mIsActive;
    };
}

#endif //SFML_TEST_COMPONENT_H
