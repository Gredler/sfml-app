//
// Created by Andi on 06.02.2018.
//

#ifndef SFML_TEST_MENUSTATE_H
#define SFML_TEST_MENUSTATE_H


#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>
#include "State.h"
#include "Container.h"


class MenuState : public State {
public:
    MenuState(StateStack &stack, Context context);

    virtual void draw();

    virtual bool update(sf::Time dt);

    virtual bool handleEvent(const sf::Event &event);


private:
    sf::Sprite mBackgroundSprite;
    GUI::Container mGUIContainer;
};


#endif //SFML_TEST_MENUSTATE_H
