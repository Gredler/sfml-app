//
// Created by Andi on 06.02.2018.
//

#ifndef SFML_TEST_PAUSESTATE_H
#define SFML_TEST_PAUSESTATE_H


#include "State.h"
#include "Container.h"

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>


class PauseState : public State {
public:
    PauseState(StateStack &stack, Context context);

    virtual void draw();

    virtual bool update(sf::Time dt);

    virtual bool handleEvent(const sf::Event &event);


private:
    sf::Sprite mBackgroundSprite;
    sf::Text mPausedText;
    GUI::Container mGUIContainer;
};


#endif //SFML_TEST_PAUSESTATE_H
