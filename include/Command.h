//
// Created by Andi on 05.02.2018.
//

#ifndef SFML_TEST_COMMAND_H
#define SFML_TEST_COMMAND_H

#endif //SFML_TEST_COMMAND_H

#include "Category.h"

#include <SFML/System/Time.hpp>

#include <functional>
#include <cassert>


class SceneNode;

struct Command {
    Command();

    std::function<void(SceneNode &, sf::Time)> action;
    unsigned int category;
};

template<typename GameObject, typename Function>
std::function<void(SceneNode &, sf::Time)> derivedAction(Function fn) {
    return [=](SceneNode &node, sf::Time dt) {
        // Check if cast is safe
        assert(dynamic_cast<GameObject *>(&node) != nullptr);

        // Downcast node and invoke function on it
        fn(static_cast<GameObject &>(node), dt);
    };
}