//
// Created by Andi on 02.02.2018.
//

#ifndef SFML_TEST_TEXTURES_H
#define SFML_TEST_TEXTURES_H

// Forward declaration of SFML classes
namespace sf {
    class Texture;

    class Font;
}

namespace Textures {
    enum ID {
        Eagle,
        Raptor,
        Desert,
        TitleScreen,
        ButtonNormal,
        ButtonSelected,
        ButtonPressed
    };
}

namespace Fonts {
    enum ID {
        Main,
    };
}

// Forward declaration and a few type definitions
template<typename Resource, typename Identifier>
class ResourceHolder;

typedef ResourceHolder<sf::Texture, Textures::ID> TextureHolder;
typedef ResourceHolder<sf::Font, Fonts::ID> FontHolder;


#endif //SFML_TEST_TEXTURES_H