//
// Created by Andi on 12.02.2018.
//

#include "../include/Label.h"
#include "../include/Utility.h"

#include "../include/ResourceHolder.h"

#include <SFML/Graphics/RenderTarget.hpp>


namespace GUI {

    Label::Label(const std::string &text, const FontHolder &fonts)
            : mText(text, fonts.get(Fonts::Main), 16) {
    }

    bool Label::isSelectable() const {
        return false;
    }

    void Label::handleEvent(const sf::Event &) {
    }

    void Label::draw(sf::RenderTarget &target, sf::RenderStates states) const {
        states.transform *= getTransform();
        target.draw(mText, states);
    }

    void Label::setText(const std::string &text) {
        mText.setString(text);
    }

}