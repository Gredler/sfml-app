cmake_minimum_required(VERSION 3.7)
set(PROJECT_NAME sfml-test)

project(${PROJECT_NAME})
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -static")

set(CMAKE_CXX_STANDARD 14)

set(SOURCE_FILES source/main.cpp source/Application.cpp include/Application.h include/ResourceHolder.inl include/ResourceHolder.h include/ResourceIdentifiers.h source/Entity.cpp include/Entity.h source/Aircraft.cpp include/Aircraft.h source/SceneNode.cpp include/SceneNode.h source/World.cpp include/World.h source/SpriteNode.cpp include/SpriteNode.h include/Command.h include/Category.h source/CommandQueue.cpp include/CommandQueue.h source/Player.cpp include/Player.h source/Command.cpp source/StateStack.cpp include/StateStack.h source/State.cpp include/State.h include/StateIdentifiers.h source/GameState.cpp include/GameState.h source/TitleState.cpp include/TitleState.h source/MenuState.cpp include/MenuState.h source/LoadingState.cpp include/LoadingState.h source/ParallelTask.cpp include/ParallelTask.h include/Utility.inl include/Utility.h source/PauseState.cpp include/PauseState.h source/Utility.cpp source/Component.cpp include/Component.h source/Container.cpp include/Container.h source/Label.cpp include/Label.h source/Button.cpp include/Button.h source/SettingsState.cpp include/SettingsState.h)

if (CMAKE_BUILD_TYPE STREQUAL "Release")
    add_executable(${PROJECT_NAME} WIN32 ${SOURCE_FILES})
else ()
    add_executable(${PROJECT_NAME} ${SOURCE_FILES})
endif ()

set(SFML_ROOT "${CMAKE_CURRENT_LIST_DIR}/sfml")
set(SFML_STATIC_LIBRARIES TRUE)

set(CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/cmake_modules")
find_package(SFML 2 REQUIRED COMPONENTS audio graphics window system)
if (SFML_FOUND)
    include_directories(${SFML_INCLUDE_DIR})
    target_link_libraries(${PROJECT_NAME} ${SFML_LIBRARIES} ${SFML_DEPENDENCIES})
endif ()

FILE(COPY media/textures/sky.png DESTINATION "${CMAKE_BINARY_DIR}/media/textures")
FILE(COPY media/textures/charizard_back.png DESTINATION "${CMAKE_BINARY_DIR}/media/textures")
FILE(COPY media/textures/salamence_back.png DESTINATION "${CMAKE_BINARY_DIR}/media/textures")
FILE(COPY media/textures/TitleScreen.png DESTINATION "${CMAKE_BINARY_DIR}/media/textures")
FILE(COPY media/textures/ButtonNormal.png DESTINATION "${CMAKE_BINARY_DIR}/media/textures")
FILE(COPY media/textures/ButtonPressed.png DESTINATION "${CMAKE_BINARY_DIR}/media/textures")
FILE(COPY media/textures/ButtonSelected.png DESTINATION "${CMAKE_BINARY_DIR}/media/textures")
FILE(COPY media/Sansation.ttf DESTINATION "${CMAKE_BINARY_DIR}/media")
